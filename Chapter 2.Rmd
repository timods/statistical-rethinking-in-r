---
title: "Chapter 2 - Small Worlds and Large Worlds"
output: html_notebook
---

# Intro

- The *small world* is the self-contained logical world of the model. within it, all possibilities are nominated and there are no pure surprises.
- the *large world* is the broader context into which the model is deployed. Here there may be events that were not imagined in the small world. Moreover, the small world is always an incomplete representation of the large world.

```{r}
library(rethinking)
```
## The Garden of Forking Data
### From counts to probabilities

$\text{plausibility of p after } D_{new} = \frac{\text{ways p can produce } D_{new} * \text{ prior plausibility p}}{\text{sum of products}}$

```{r}
ways <- c(0, 3, 8, 9, 0)
ways/sum(ways)
```

## Building a model

Suppose we toss a globe of our planet and mark whether our right index finger is on water $W$ or land $L$, and that we see the following result: \[WLWWWLWLW\] (six water, three land)..

### The data story

1. The true proportion of water covering the globe is $p$
2. A single toss of the globa has a probability $p$ of producing a water observation. It has probability $1-p$ of producing a land observation
3. Each toss of the globe is independent of the others

### Components of the model

1. The likelihood function
2. One or more parameters
3. A prior

#### Likelihood

The binomial distribution is a natural fit given our data story above:
\[Pr(w|n,p)=\frac{n!}{w!(n-w)!}p^w(1-p)^{n-w}\]

Read this in english as:

> The count of "water" observations $w$ is distributed binomially, with probability $p$ of "water" on each toss and $n$ tosses total

The likelihood of this data given a probability $p=0.5$ can be computed in R as:
```{r}
dbinom(6, size=9, prob=0.65)
```

$Pr(w|n, p)$ is the same as $L(p|w,n)$, but written differently.

*R notes*: "d" in $dbinom$ stands for "density". "r" stands for "random sample", and "p" for "cumulative probabilities".

### Parameters
For the binomial likelihood these inputs are
1. $p$, the probability of observing a $W$
2. $n$, the sample size
3. $w$, the number of $W$s observed

In our example both $n$ and $w$ are data, leaving $p$ as the parameter we wish to learn about.

Some common questions we'll ask that can be directly answered by our parameters:

- What is the average difference between treatment groups?
- How strong is the association between a treatment and an outcome?
- Does the effect of the treatment depend upon a covariate?
- How much variation is there among groups?

### Prior

For every parameter we must provide a prior probability distribution, which are engineering assumptions that help the machine learn. They're useful for constraining parameters to reasonable ranges, as well as for expressing any knowledge we have about the parameters.

Here we know that both $p=0$ and $p=1$ are completely implausible, and we also know that values of $p$ near $0$ and $1$ are less plausible than values near $p=0.5$. 

### Posterior

The posterior is a purely logical consequence of the assumptions made for the likelihood, parameters, and prior. This is defined by *Baye's Theorem*: \[Pr(p|w)=\frac{Pr(w|p)Pr(p)}{Pr(w)}\]

In words: \[\text{Posterior} = \frac{\text{Likelihood}\times\text{Prior}}{\text{Average Likelihood}}\]

where the denominator is the *marginal likelihood*: \[Pr(w)=E[Pr(w|p)] = \int Pr(w|p)Pr(p)dp\]

## Making our model go

Three example numerical techniques for computing posterior distributions:

1. Grid approximation
2. Quadratic approximation
3. Markov chain Monte Carlo (MCMC)

#### Grid approximation

1. Define the grid. How many points are estimated?
2. Compute the value fo the prior at each parameter value on the grid
3. Compute the likelihood at each parameter value
4. Compute the unstandardized posterior at each parameter value by multiplying the prior by the likelihood
5. Standardize the posterior by dividing each value by the sum of all values

Let's try this with a uniform prior:
```{r}
p_grid <- seq(from=0, to=1, length.out=20)
prior <- rep(1, 20)
likelihood <- dbinom(6, size=9, prob=p_grid)
unstd.posterior <- likelihood * prior
posterior <- unstd.posterior / sum(unstd.posterior)
plot(p_grid, posterior, type="b", xlab="probability of water", ylab="posterior probability" )
mtext("20 points, uniform prior")
```
```{r}
p_grid <- seq(from=0, to=1, length.out=20)
prior <- ifelse(p_grid < 0.5, 0, 1)
likelihood <- dbinom(6, size=9, prob=p_grid)
unstd.posterior <- likelihood * prior
posterior <- unstd.posterior / sum(unstd.posterior)
plot(p_grid, posterior, type="b", xlab="probability of water", ylab="posterior probability" )
mtext("20 points, step prior")
```

```{r}
p_grid <- seq(from=0, to=1, length.out=20)
prior <- exp(-5*abs(p_grid-0.5))
likelihood <- dbinom(6, size=9, prob=p_grid)
unstd.posterior <- likelihood * prior
posterior <- unstd.posterior / sum(unstd.posterior)
plot(p_grid, posterior, type="b", xlab="probability of water", ylab="posterior probability" )
mtext("20 points, weird third prior")
```

#### Quadratic approximation

Grid approximations don't scale to large numbers of parameters (or values). Under quite general conditions, the regions near the peak of the posterior distribution will be nearly Gaussian, meaning we can approximate it using a Gaussian distribution. A Gaussian approximation is called "quadratic approximation" because the logarithm in a Gaussian distribution forms a parabola, which is a quadratic function.

This procedure involves two steps:

1. Find the posterior mode, which is usually accomplished by some optimization algorithm.
2. Estimate the curvature near the mode

The `rethinking` package helps us with this:

```{r}
library(rethinking)
globe.qa <- map(
  alist(
    w ~ dbinom(9, p), # binomial likelihood
    p ~ dunif(0, 1)   # uniform prior
  ),
  data=list(w=6) )
# display summary of quadratic approximation
precis(globe.qa)
```

The above table can be read as _Assuming the posterior is Gaussian, it is maximixed at 0.67 and its standard deviation is 0.16_.

The analytical calculation is:
```{r}
w <- 6
n <- 9
curve(dbeta(x, w+1, n-w+1), from=0, to=1)
curve(dnorm(x, 0.67, 0.16), lty=2, add=TRUE)
mtext("Accuracy of the quadratic approximation")
```
The estimate improves with more data.

#### MCMC

It's cool and we'll get to it later. Instead of computing or approximating the posterior distribution directly, MCMC techniques draw samples from it. We end up with a collection of parameter values, and the frequencies of theese correspond to the posterior probabilities!


## Practice
**Easy.**

**2E1** $Pr(\text{rain}|\text{Monday})$

**2E2** $Pr(\text{Monday}|\text{rain})$ is "The probability that it is Monday, given that it is raining".

**2E3** (1)

**2E4** "the probability of water is 0.7" means that were we to toss the globe and track where our right index finger landed, there would be a 70% chance that our finger would be touching water rather than land. It is our approximation for the percentage of the Earth's surface that is covered in water.

**Medium.**

**2M1** 

1. $W,W,W$
```{r}
p_grid <- seq(from=0, to=1, length.out=20)
prior <- rep(1, 20)
likelihood <- dbinom(3, size=3, prob=p_grid)
unstd.posterior <- likelihood * prior
posterior <- unstd.posterior / sum(unstd.posterior)
plot(p_grid, posterior, type="b", xlab="probability of water", ylab="posterior probability" )
mtext("20 points, uniform prior for WWW")
```


2. $W,W,W,L$
```{r}
p_grid <- seq(from=0, to=1, length.out=20)
prior <- rep(1, 20)
likelihood <- dbinom(3, size=4, prob=p_grid)
unstd.posterior <- likelihood * prior
posterior <- unstd.posterior / sum(unstd.posterior)
plot(p_grid, posterior, type="b", xlab="probability of water", ylab="posterior probability" )
mtext("20 points, uniform prior for WWWL")
```
3. $L,W,W,L,W,W,W$
```{r}
p_grid <- seq(from=0, to=1, length.out=20)
prior <- rep(1, 20)
likelihood <- dbinom(5, size=7, prob=p_grid)
unstd.posterior <- likelihood * prior
posterior <- unstd.posterior / sum(unstd.posterior)
plot(p_grid, posterior, type="b", xlab="probability of water", ylab="posterior probability" )
mtext("20 points, uniform prior for LWWLWWW")
```

**2M2** All of the above but with the following prior: `ifelse(p_grid<0.5, 0, 1)`.

1. $W,W,W$
```{r}
p_grid <- seq(from=0, to=1, length.out=20)
prior <-ifelse(p_grid<0.5, 0, 1)
likelihood <- dbinom(3, size=3, prob=p_grid)
unstd.posterior <- likelihood * prior
posterior <- unstd.posterior / sum(unstd.posterior)
plot(p_grid, posterior, type="b", xlab="probability of water", ylab="posterior probability" )
mtext("20 points, step prior for WWW")
```

2. $W,W,W,L$
```{r}
p_grid <- seq(from=0, to=1, length.out=20)
prior <-ifelse(p_grid<0.5, 0, 1)
likelihood <- dbinom(3, size=4, prob=p_grid)
unstd.posterior <- likelihood * prior
posterior <- unstd.posterior / sum(unstd.posterior)
plot(p_grid, posterior, type="b", xlab="probability of water", ylab="posterior probability" )
mtext("20 points, step prior for WWWL")
```

3. $L,W,W,L,W,W,W$
```{r}
p_grid <- seq(from=0, to=1, length.out=20)
prior <-ifelse(p_grid<0.5, 0, 1)
likelihood <- dbinom(5, size=7, prob=p_grid)
unstd.posterior <- likelihood * prior
posterior <- unstd.posterior / sum(unstd.posterior)
plot(p_grid, posterior, type="b", xlab="probability of water", ylab="posterior probability" )
mtext("20 points, step prior for LWWLWWW")
```

**2M3** Suppose there are two globes, one for Earth and one for Mars. The Earth globe is 70% covered in water, and the Mars globe is 100% land. Further, suppose that one of these globes---you don't know which--- was tossed in the air and produced a "land" observation. Assume that each globe was equally likely to be tossed. Show that the posterior probability that the globe was the Earth, conditional on seeing land, is 0.23

```{r}
pr.earth <- 0.5
pr.mars <- 1.0 - pr.earth
pr.water.earth <- 0.7
pr.land.earth <- 1 - pr.water.earth # = pr(earth|land) * pr(land) / pr(earth)
pr.land.mars <- 1.0
# land can arise 70% of the time 50% of the time (earth), and 100% of the time 50% of the time (mars)
pr.land = pr.earth * pr.land.earth + pr.mars * pr.land.mars  
pr.earth.land = pr.land.earth * pr.earth / pr.land
all.equal(pr.earth.land, 0.23, 0.01)
```

**2M4** Given a deck of three cards with two sides each: BB, BW, WW. A card with a black side is drawn, but we don't know the other side. Show that the probability that the other side is also black is 2/3 using the counting method

Given that we've drawn a black side, there are 2 ways to draw the first card out of 3 possible draws that result in black.
Therefore, the probability that the other side is also black is the probability that we've drawn the first card, or 2/3

**2M5** Suppose now we have four cards: BB, BW, WW, BB. What's the probability of drawing BB?

When we've drawn a black card, there are 4 ways to draw BB and 1 way to draw BW
Therefore, the probability that we've drawn a BB card is 4/5.

**2M6** Our deck is BB, BW, WW and black sides are heavier than white sides so are less likely to be drawn face up.

- 1 way to pull BB
- 2 ways to pull BW
- 3 ways to pull WW

Suppose we've pulled a card with the black side face up. What's the probability that the other side is also black?
Let's pretend we've a larger deck that makes this explicit: 1xBB, 2xBW, 3xWW
Given that we've drawn a black side face-up, there are 2 ways to have pulled BW and 2 ways to have pulled the BB card.
Therefore, the probability that the other side is black is 2/4 = 1/2

**2M7** Back to the original problem: BB, BW, WW. We draw a single card with a black side facing up, then draw a second card with a white side facing up. Show that the probability that the first card has black on its second side?

- 2 ways to draw BB
  - 0 ways to draw BB
  - 1 way to draw BW
  - 2 ways to draw WW
  
There are 6 possible states if we've drawn BB

- 1 way to draw BW  
  - 0 ways to draw BB
  - 0 ways to draw BW
  - 2 ways to draw WW

There are 2 possible states if we've drawn BW

Probability of BB being the first card is 6 ways/8 ways = 0.75

**Hard.**

**2H1** 
```{r}
p.a = 0.5
p.b = 1.0 - p.a
p.twins.a = 0.1
p.twins.b = 0.2

p.twins = p.a * p.twins.a + p.b * p.twins.b
# P(a|twins)
p.a.twins = p.twins.a * p.a / p.twins
p.b.twins = p.twins.b * p.b / p.twins
# P(twins|twins) = p(twins|a) * p(a|twins) + p(twins|b) * p(b|twins)
p.twins.twins = p.twins.a * p.a.twins + p.twins.b * p.b.twins
print(p.twins.twins)
```

**2H2.**

```{r}
p.a.twins
```

**2H3.** 
```{r}
p.solo.a = 1 - p.twins.a
p.solo.b = 1 - p.twins.b
p.solo = p.solo.a * p.a + p.solo.b * p.b
#p.solo.twins = p.solo.a * p.a.twins + p.solo.b * p.b.twins
p.solo_twins = p.solo * p.twins
p.solo_twins.a = p.solo.a * p.twins.a
p.a.solo_twins = p.solo_twins.a * p.a / p.solo_twins
p.a.solo_twins
```

**2H4.** 
```{r}
# Without birth data
p.ta.a = 0.8
p.tb.b = 0.65
p.ta.b = 1 - p.tb.b
p.tb.a = 1 - p.ta.a
# The denominator is all possible ways to produce an A result from the test
p.a.ta.nobirth = (p.ta.a / (p.ta.b + p.ta.a))
p.a.ta.nobirth

# With birth data
p.ta = p.ta.a * p.a + p.ta.b * p.b
p.a.ta = p.ta.a * p.a / p.ta
p.a.ta

p.a.ta.nobirth == p.a.ta
```